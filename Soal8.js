const getInfoPenjualan = (dataPenjualan) => {
    let currency = new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR'
      });
      
    let infoPenjualan = {
        totalKeuntungan: 0, 
        totalModal: 0, 
        persentaseKeuntungan: null, 
        produkBukuTerlaris: null,
        penulisTerlaris: null
    };

    if (dataPenjualan != null){
        if (typeof dataPenjualan == "object"){
            let dataTerlaris = dataPenjualan.sort((a, b) => {
                return b.totalTerjual - a.totalTerjual;
            })[0];

            dataPenjualanNovel.map((data, index) => {
                infoPenjualan.totalKeuntungan += (data.hargaJual - data.hargaBeli) *  data.totalTerjual;
                infoPenjualan.totalModal += data.hargaBeli * (data.totalTerjual + data.sisaStok);
            });

            infoPenjualan.persentaseKeuntungan = (Math.round(((
                infoPenjualan.totalKeuntungan/infoPenjualan.totalModal
                ) * 100) * 100) / 100) + "%";
                
            infoPenjualan.totalKeuntungan = currency.format(infoPenjualan.totalKeuntungan);
            infoPenjualan.totalModal = currency.format(infoPenjualan.totalModal);
            infoPenjualan.produkBukuTerlaris = dataTerlaris.namaProduk;
            infoPenjualan.penulisTerlaris = dataTerlaris.penulis;

            return infoPenjualan;
        }else{
            return "Error: Invalid data type";
        }
    }else{
        return "Error: Bro where is the parameter?";
    } 
}

const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
];

console.log(getInfoPenjualan(dataPenjualanNovel));