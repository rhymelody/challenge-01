const isValidPassword = (givenPassword) => { 
    const regex = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$";
    if (givenPassword != null){
        if (typeof givenPassword == "string"){
            if (givenPassword.match(regex)){
                return true;
            }else{
                return false;
            }
        }else{
            return "Error: Invalid data type";
        }
    }else{
        return "Error: Bro where is the parameter?";
    }  
}

console.log(isValidPassword("Meong2021"));
console.log(isValidPassword("meong2021"));
console.log(isValidPassword("@eong"));
console.log(isValidPassword("Meong2"));
console.log(isValidPassword(0));
console.log(isValidPassword());