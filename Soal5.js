const getSplitName = (personName) => { 
    let getSplit = personName.toString().split(" ");
    let splittedName = {
        firstName: null,
        middleName: null,
        lastName: null
    }

    if (personName != null){
        if (typeof personName == "string"){
            if (getSplit.length <= 3){
                getSplit.map((value, index) => {
                    let key = Object.keys(splittedName)[
                        (index == 1 && getSplit.length <= 2 ) ? ++index : index
                    ];
                    splittedName[key] = value;
                })
                return splittedName;
            }else{
                return "Error : This function is only for 3 characters name";
            }
        }else{
            return "Error: Invalid data type";
        }
    }else{
        return "Error: Bro where is the parameter?";
    }  
}

console.log(getSplitName("Aldi Daniela Pranata"));
console.log(getSplitName("Dwi Kuncoro"));
console.log(getSplitName("Aurora"));
console.log(getSplitName("Aurora Aureliya Sukma Darma"));
console.log(getSplitName(0));