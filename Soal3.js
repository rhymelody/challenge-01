const checkEmail = (email) =>{ 
    const regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (email){
        if (typeof email == "string"){
            if (!(email.search("@") == -1)){
                if (email.toLowerCase().match(regex)){
                    return "VALID";
                }else{
                    return "INVALID";
                }
            }else{
                return "Error: Email should contain a @ symbol";
            }
        }else{
            return "Error: Invalid data type";
        }
    }else{
        return "Error: Bro where is the parameter?";
    }   
}

console.log(checkEmail("apranata@binar.co.id")); 
console.log(checkEmail("apranata@binar.com"));
console.log(checkEmail("apranata@binar"));
console.log(checkEmail("apranata"));
console.log(checkEmail(3322));
console.log(checkEmail());