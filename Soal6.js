const getAngkaTerbesarKedua = (dataNumbers) => { 
    if (dataNumbers != null){
        if (typeof dataNumbers == "object"){
            return dataNumbers.sort((a, b) => {return b - a})[1];
        }else{
            return "Error: Invalid data type";
        }
    }else{
        return "Error: Bro where is the parameter?";
    }  
}

const dataAngka = [9,4,7,7,4,3,2,2,8];

console.log(getAngkaTerbesarKedua(dataAngka));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());